package com.laim2003.mre.anothermodule

/**
 * MODULE A
 * This class provides a text to the main app module
 */
class ModuleAExampleClass {
    /**
     * get the module example text
     */
    fun getText() = "module B text!"
}