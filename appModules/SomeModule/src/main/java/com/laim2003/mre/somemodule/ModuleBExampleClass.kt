package com.laim2003.mre.somemodule

/**
 * MODULE B
 * This class provides a text to the main app module
 */
class ModuleBExampleClass {
    /**
     * get the module example text
     */
    fun getText() = "module B text!"
}