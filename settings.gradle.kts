include("app")

include(":AnotherModule")
project(":AnotherModule").projectDir = File(rootDir, "appModules/AnotherModule")

include(":SomeModule")
project(":SomeModule").projectDir = File(rootDir, "appModules/SomeModule")