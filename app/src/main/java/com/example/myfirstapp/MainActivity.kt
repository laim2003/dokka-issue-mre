package com.example.myfirstapp;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.laim2003.mre.anothermodule.ModuleAExampleClass
import com.laim2003.mre.somemodule.ModuleBExampleClass
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val moduleAExampleClass = ModuleAExampleClass()
        val moduleBExampleClass = ModuleBExampleClass()
        mainText.text = buildString {
            append(moduleAExampleClass.getText())
            append("\n")
            append(moduleBExampleClass.getText())
        }
    }
}
