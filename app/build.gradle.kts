apply(from = rootProject.file("base.gradle"))

plugins {
    id("com.android.application")
}
android {
    defaultConfig {
        applicationId = "com.laim2003.mre"
    }
    useLibrary("android.test.base")
    useLibrary("android.test.mock")
}
dependencies {
    implementation(project(":SomeModule"))
    implementation(project(":AnotherModule"))
}


tasks {
    val dokka by getting(org.jetbrains.dokka.gradle.DokkaTask::class) {
        println("loading dokka config for module:app")

        subProjects = listOf(":AnotherModule", ":SomeModule")

        println(subProjects)
    }
}

